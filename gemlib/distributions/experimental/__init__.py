"""Experimental gemlib distributions"""

from gemlib.distributions.experimental.discrete_approx_cont_state_transition_model import (  # noqa: E501
    DiscreteApproxContStateTransitionModel,
)

__all__ = ["DiscreteApproxContStateTransitionModel"]
