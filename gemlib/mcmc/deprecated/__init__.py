"""Deprecated samplers

The samplers in this sub-module are deprecated, though retained for
backwards compatibility.  They are not included in code-quality metrics.
Use at your own risk.
"""
