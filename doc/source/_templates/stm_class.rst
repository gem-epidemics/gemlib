{{ objname | escape | underline}}

.. currentmodule:: {{ module }}

.. autoclass:: {{ objname }}
	       
   {% block methods %}
   .. automethod:: __init__

   {% if methods %}

   .. rubric:: {{ _('Methods') }}

   .. autosummary::
      :toctree: classmethods
      :nosignatures:

   {% for item in state_transition_model_methods %}
       ~{{ name }}.{{ item }}
   {%- endfor %}
   {% endif %}
   {% endblock %}

   {% block attributes %}
   {% if attributes %}
   .. rubric:: {{ _('Atributes') }}

   .. autosummary::
   {% for item in attributes %}
      ~{{ name }}.{{ item }}
   {%- endfor %}
   {% endif %}
   {% endblock %}
