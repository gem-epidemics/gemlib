API reference
=============

State transition models
-----------------------

The state transition model classes are used to implement models in which
individuals are considered to transition between a finite set of mutually-
exclusive states.


.. currentmodule:: gemlib.distributions
		   
.. autosummary::
   :toctree: distributions/
   :template: stm_class.rst

   ContinuousTimeStateTransitionModel
   DeterministicStateTransitionModel
   DiscreteTimeStateTransitionModel
 

MCMC kernels
------------

.. currentmodule:: gemlib.mcmc

`Markov-chain Monte Carlo (MCMC)
<https://en.wikipedia.org/wiki/Markov_chain_Monte_Carlo>`_ is an algorithm used for
drawing from random variables when the probability density function is known only up to a
normalising constant.  This makes MCMC appropriate for sampling from complex Bayesian
posteriors.

The Gemlib MCMC library is arranged into a number of parts as follows.

MCMC drivers
++++++++++++

Kernel drivers are used to guide the overall MCMC scheme. We provide
functionality for iterating the MCMC (:func:`mcmc`), restricting a
kernel to operate on a subset of the parameter space as part of a
Metropolis-within-Gibbs scheme (:class:`MwgStep`), and a special
"meta"-kernel (:func:`multi_scan`) that can invoke a kernel multiple
times within an overall Metropolis-within-Gibbs scheme.

.. autosummary::
   :toctree: mcmc/

   MwgStep
   SamplingAlgorithm
   mcmc
   multi_scan
   

Atomic kernels
++++++++++++++

Atomic kernels encapsuate algorithms that propagate the Markov chain, i.e.  an MCMC
"step".  Each function returns an instance of a :class:`~gemlib.mcmc.SamplingAlgorithm`,
which is a tuple of :meth:`~gemlib.mcmc.SamplingAlgorithm.init` and
:meth:`~gemlib.mcmc.SamplingAlgorithm.step` functions with the special property of being
composable.

Here's an example of creating a single Hamiltonian Monte-Carlo sampler::

  from gemlib.mcmc import hmc

  def log_prob_fn(x):
    return sp.stats.normal.logpdf(x, loc=0.0, scale=1.0)

  kernel = hmc(step_size=0.1, num_leapfrog_steps=16)

  # Draw samples
  samples, results = mcmc(
      num_samples=1000,
      sampling_algorithm=kernel,
      target_density_fn=log_prob_fn,
      initial_position=0.1,
      seed=[0,0],
  )

The major novelty in Gemlib is that kernels may be composed,
such that appropriate methods can be
used to propagate different parts of the parameter set as part of a
Metropolis-within-Gibbs algorithm.  For example::

  from gemlib.mcmc import MwgStep, hmc, rwmh

  def log_prob_fn(x, y):
    x1_pdf = sp.stats.normal.logpdf(x, loc=0.0, scale=1.0)
    x2_pdf = sp.stats.normal.logpdf(y, loc=1.0, scale=2.0)

  Position = namedtuple("Position", ["x", "y"])
    
  kernel1 = MwgStep(
      hmc(step_size=0.1, num_leapfrog_steps=16),
      target_names=["x"],
  )
  
  kernel2 = MwgStep(
      rwmh(scale=1.0),
      target_names=["y"],
  )

  # Compose the kernels
  kernel = kernel1 >> kernel2

  # Draw samples
  samples, results = mcmc(
      num_samples=1000,
      sampling_algorithm=kernel,
      target_density_fn=log_prob_fn,
      initial_position=Position(0.1, 0.2),
      seed=[0,0],
  )


.. autosummary::
    :toctree: mcmc/

    adaptive_rwmh
    hmc
    rwmh
    transform_sampling_algorithm

.. currentmodule:: gemlib.mcmc.discrete_time_state_transition_model



Discrete-time state transition model kernels
++++++++++++++++++++++++++++++++++++++++++++

These kernels operate on censored event times generated from the
:class:`~gemlib.distributions.DiscreteTimeStateTransitionModel` class.

.. autosummary::
   :toctree: mcmc/discrete_time_state_transition_model/
      
    left_censored_events_mh
    move_events
    right_censored_events_mh

